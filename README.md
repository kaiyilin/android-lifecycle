# README #

### 目的 ###

* 介紹＆應用 Android JetPack Lifecycle

### 說明 ###

* LoginActivity、MainActivity、RegisterActivity 所有的Activity所有的生命週期函數，都可以被LifestyleObserver去監聽
* CustomLifecycle是觀察者角色
* LoginActivity、MainActivity、RegisterActivity 是被觀察者角色
* 綁定：lifecycle.addObserver(CustomLifecycle())
* 還沒有很明顯Lifecycle的魅力，為什麼  答：就是監聽Activity、Fragment 所有生命週期函數，感覺沒什麼用處
* 透過demo1、demo2、demo3來逐步證明Android JetPack Lifecycle的魅力

### 總結 ###

* Lifecycle真正好處：
* demo1：如果有100多個activity或fragment 所有生命週期函數的的業務or功能邏輯，有可能開發人員忘了寫，就會引發一致性問題
* demo2：通過接口解決"一致性問題"，其實就是把業務or功能邏輯，寫到了GPSEngineManager裡面，統一管理，一致性問題就解決了，但程式碼耦合性增加、還可能引發內存泄露問題
* demo3：觀察100多個activity或fragment 所有生命週期函數，由於是觀察，所以沒有耦合性、沒有內存泄露
* 監聽了整個App的所有Activity生命週期函數，當執行了onCreate，觀察者觀察到了onCreate函數的話，可以做事情