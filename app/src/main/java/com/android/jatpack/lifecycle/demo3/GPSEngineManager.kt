package com.android.jatpack.lifecycle.demo3

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

/****************************************************************
 * Copyright (C) Kevin Corporation. All rights reserved.
 *
 * Author: Kevin Lin
 * Create Date: 2020/9/12
 * Usage:
 *
 **************************************************************/

//觀察者
class GPSEngineManager : LifecycleObserver {

    //是否存活
    var isAlive = false

    private object GPSEngineManagerHolder {
        val GPSEngineManager = GPSEngineManager()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME) //可觀察100多個Activity
    fun onResumeAction() {
        Log.e(this.javaClass.simpleName, "啟用GPS定位")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE) //可觀察100多個Activity
    fun onPauseAction() {
        isAlive = true
        Log.e(this.javaClass.simpleName, "停用GPS定位")
    }

    companion object {
        val instance = GPSEngineManagerHolder.GPSEngineManager
    }
}

