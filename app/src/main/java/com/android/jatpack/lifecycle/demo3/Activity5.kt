package com.android.jatpack.lifecycle.demo3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.android.jatpack.lifecycle.R

class Activity5 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_5)
        lifecycle.addObserver(GPSEngineManager.instance)
    }

}