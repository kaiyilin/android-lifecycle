package com.android.jatpack.lifecycle.demo1

import android.util.Log

/****************************************************************
 * Copyright (C) Kevin Corporation. All rights reserved.
 *
 * Author: Kevin Lin
 * Create Date: 2020/9/12
 * Usage:
 *
 **************************************************************/

//GPS 很耗電
class GPSEngineManager {

    //是否存活
    var isAlive = false

    private object GPSEngineManagerHolder {
        val GPSEngineManager = GPSEngineManager()
    }

    fun onResumeAction() {
        Log.e(this.javaClass.simpleName, "啟用GPS定位")
    }

    fun onPauseAction() {
        Log.e(this.javaClass.simpleName, "停用GPS定位")
    }

    companion object {
        val instance = GPSEngineManagerHolder.GPSEngineManager
    }
}

