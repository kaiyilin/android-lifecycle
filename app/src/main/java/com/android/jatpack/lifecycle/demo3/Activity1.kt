package com.android.jatpack.lifecycle.demo3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.android.jatpack.lifecycle.R

// 被觀察者
class Activity1 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_1)
        //綁定
        lifecycle.addObserver(GPSEngineManager.instance)
    }
}