package com.android.jatpack.lifecycle.demo2

import android.util.Log

/****************************************************************
 * Copyright (C) Kevin Corporation. All rights reserved.
 *
 * Author: Kevin Lin
 * Create Date: 2020/9/12
 * Usage:
 *
 **************************************************************/

//使用接口解決一致性問題
class GPSEngineManager : ICallBack {

    //是否存活
    var isAlive = false

    private object GPSEngineManagerHolder {
        val GPSEngineManager = GPSEngineManager()
    }

    /**
     * 下面做到統一管理，解決了一致性問題，把一百多個Activity業務or功能邏輯全部封裝到這邊
     */

    override fun onResumeAction() {
        //業務or功能邏輯
        isAlive = true
        Log.e(this.javaClass.simpleName, "啟用GPS定位")
    }

    override fun onPauseAction() {
        //業務or功能邏輯
        isAlive = false
        Log.e(this.javaClass.simpleName, "停用GPS定位")
    }

    companion object {
        val instance = GPSEngineManagerHolder.GPSEngineManager
    }
}

