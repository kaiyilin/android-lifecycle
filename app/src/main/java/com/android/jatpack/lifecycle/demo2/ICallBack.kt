package com.android.jatpack.lifecycle.demo2

/****************************************************************
 * Copyright (C) Kevin Corporation. All rights reserved.
 *
 * Author: Kevin Lin
 * Create Date: 2020/9/13
 * Usage:
 *
 **************************************************************/

interface ICallBack {

    fun onResumeAction()

    fun onPauseAction()
}