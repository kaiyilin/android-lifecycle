package com.android.jatpack.lifecycle

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

/****************************************************************
 * Copyright (C) Kevin Corporation. All rights reserved.
 *
 * Author: Kevin Lin
 * Create Date: 2020/9/12
 * Usage:
 *
 **************************************************************/

//觀察者的角色
//監聽Activity/Fragment所有生命週期函數
class CustomLifecycle : LifecycleObserver {

    //監聽onCreate被執行
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public fun onCreate() {
        Log.e(this.javaClass.simpleName, "onCreate")
    }

    //監聽onStart被執行
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public fun onStart() {
        Log.e(this.javaClass.simpleName, "onStart")
    }

    //監聽onResume被執行
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public fun onResume() {
        Log.e(this.javaClass.simpleName, "onResume")
    }

    //監聽onDestroy被執行
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public fun onStop() {
        Log.e(this.javaClass.simpleName, "onStop")
    }

    //監聽onPause被執行
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public fun onPause() {
        Log.e(this.javaClass.simpleName, "onPause")
    }

    //監聽onDestroy被執行
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public fun onDestroy() {
        Log.e(this.javaClass.simpleName, "onDestroy")
    }
}