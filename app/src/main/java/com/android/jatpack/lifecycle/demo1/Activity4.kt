package com.android.jatpack.lifecycle.demo1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.android.jatpack.lifecycle.R

class Activity4 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_4)
    }

    override fun onResume() {
        super.onResume()
        GPSEngineManager.instance.isAlive = true //activity 存活
        GPSEngineManager.instance.onResumeAction() // GPS打開
    }

    override fun onPause() {
        super.onPause()
        GPSEngineManager.instance.isAlive = true //activity 停止
        GPSEngineManager.instance.onPauseAction() // GPS關閉
    }
}