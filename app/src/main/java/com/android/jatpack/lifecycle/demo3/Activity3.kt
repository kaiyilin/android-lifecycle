package com.android.jatpack.lifecycle.demo3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.android.jatpack.lifecycle.R

class Activity3 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_3)
        lifecycle.addObserver(GPSEngineManager.instance)
    }
}