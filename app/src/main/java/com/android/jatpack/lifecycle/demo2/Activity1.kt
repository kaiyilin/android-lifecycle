package com.android.jatpack.lifecycle.demo2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.android.jatpack.lifecycle.R

class Activity1 : AppCompatActivity() {

    var iCallBack: ICallBack? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_1)
        iCallBack = GPSEngineManager()
    }

    override fun onResume() {
        super.onResume()
        iCallBack?.onResumeAction()
    }

    override fun onPause() {
        super.onPause()
        iCallBack?.onPauseAction()
    }
}