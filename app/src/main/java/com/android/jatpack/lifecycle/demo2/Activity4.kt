package com.android.jatpack.lifecycle.demo2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.android.jatpack.lifecycle.R

//使用接口解決“一致性問題”
//但是引發新的問題：程式碼耦合性變高了（被入侵）、還可能引發內存泄露的問題，但是解決統一管理問題
class Activity4 : AppCompatActivity() {

    var iCallBack: ICallBack? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_4)

        iCallBack = GPSEngineManager()
    }

    override fun onResume() {
        super.onResume()
        //程式碼被入侵了
        iCallBack?.onResumeAction()
        //iCallBack.onXXXX(this) - 使用接口的方式，可能會這樣寫，會把自己的this帶入接口，如果內部管理不夠好可能會引發內存泄露
    }

    override fun onPause() {
        super.onPause()
        iCallBack?.onPauseAction()
    }
}